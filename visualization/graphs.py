import tables as tb
import sys

import matplotlib.pyplot as plt

from collections import defaultdict

def get_embed_len(params):
    params = eval(params)
    return params['embed_size']

def get_nn_acc(acc, data):
    for x in data:
        name = x[0]
        nn = x[2]
        acc[name].append(nn)

def make_embed_graph(src_path):
    splits = tb.parse_splits(src_path)
    for s in splits:
        parse_dict = tb.parse_summary(s)
        tables = tb.make_summary_table(parse_dict)

        embed_sizes = []
        acc = defaultdict(list)
        for table in tables:
            params,data,headers = table
            embed_size = get_embed_len(params)
            embed_sizes.append(embed_size)
            get_nn_acc(acc, data)

        print embed_sizes
        print acc

        plt.plot(embed_sizes, acc['coffee'])





if __name__=='__main__':
    make_embed_graph(sys.argv[1])
