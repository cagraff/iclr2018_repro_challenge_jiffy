"""
Generate tables from output file.
"""

import sys
from collections import defaultdict
import tabulate

def parse_data_name(l, symbol):
    l = l[len(symbol)+2:]

    return l

def parse_nn_acc(l, symbol):
    l = l[len(symbol)+2:]

    pos = l.find('Test=') + len('Test=')
    return l[pos:]

def parse_all_acc(l, symbol):
    l = l[len(symbol)+2:]

    pos_cnn = l.find('Test=') + len('Test=')
    pos_cnn_end = pos_cnn + l[pos_cnn:].find(' ')
    cnn_acc = l[pos_cnn:pos_cnn_end]

    l = l[pos_cnn_end+len(' '):]

    pos_nn = l.find('Test=') + len('Test=')
    nn_acc = l[pos_nn:]

    return cnn_acc, nn_acc

def parse_params(l, symbol):
    l = l[len(symbol)+2:]

    return l

SYMBOLS = [('params', '#'), ('data', '$'), ('nn_data', '^'), ('nn_accuracy', '&'), ('accuracy', '@'), ('data_params', '+')]
SYMBOL_PARSERS = {
        '$': parse_data_name,
        '^': parse_data_name,
        '&': parse_nn_acc,
        '@': parse_all_acc,
        '#': parse_params,
        '+': parse_params
    }

def parse_splits(src_path):
    with open(src_path, 'r') as fin:
        splits = fin.read().split('***')

    lines_list = []
    for s in splits[1:]:
        lines_list.append(s.split('\n'))

    return lines_list

def parse_summary(lines):
    parse_dict = defaultdict(list) 

    for l in lines:
        for name,s in SYMBOLS:
            if l.startswith(s): 
                try: p = SYMBOL_PARSERS[s](l, s)
                except KeyError: p = l
                parse_dict[name].append(p)

    return parse_dict

def prep_parse_dict(parse_dict):
    params_set = set(parse_dict['params'])

    lookup_dict_dict = defaultdict(dict)
    for p,d,a in zip(parse_dict['params'], parse_dict['data'], parse_dict['accuracy']):
        lookup_dict_dict[p][d] = a

    lookup_dict_nn = defaultdict(int)
    for d,a in zip(parse_dict['nn_data'], parse_dict['nn_accuracy']):
        lookup_dict_nn[d] = a

    return lookup_dict_dict, lookup_dict_nn

def make_summary_table(parse_dict):
    tables = []
    lookup_dict_dict, lookup_dict_nn = prep_parse_dict(parse_dict)

    headers = ['dataset', '1nn (input)', 'CNN', '1nn (embed)']
    for params,data_dict in lookup_dict_dict.iteritems():
        table = []
        for k,v in data_dict.iteritems():
            table.append([k, lookup_dict_nn[k], v[0], v[1]])

        tables.append((params, table, headers))

    return tables


if __name__=='__main__':
    splits = parse_splits(sys.argv[1])
    for s in splits:
        parse_dict = parse_summary(s)
        tables = make_summary_table(parse_dict)

        print parse_dict['data_params']
        for p,t,h in tables:
            print p
            print tabulate.tabulate(t, headers=h)        
            print

